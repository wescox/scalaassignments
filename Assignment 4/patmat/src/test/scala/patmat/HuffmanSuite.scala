package patmat

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import patmat.Huffman._

@RunWith(classOf[JUnitRunner])
class HuffmanSuite extends FunSuite {
	trait TestTrees {
		val t1 = Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5)
		val t2 = Fork(Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5), Leaf('d',4), List('a','b','d'), 9)
    val leaflist = List(Leaf('e', 2), Leaf('t', 2), Leaf('x', 3))
    val leaflist2 = List(Leaf('e', 2), Leaf('t', 2), Leaf('x', 3), Leaf('d', 5), Leaf('b', 7))
	}


  test("weight of a larger tree") {
    new TestTrees {
      assert(weight(t1) === 5)
    }
  }


  test("chars of a larger tree") {
    new TestTrees {
      assert(chars(t2) === List('a','b','d'))
    }
  }


  test("string2chars(\"hello, world\")") {
    assert(string2Chars("hello, world") === List('h', 'e', 'l', 'l', 'o', ',', ' ', 'w', 'o', 'r', 'l', 'd'))
  }

  test("times of 'a b a c a a d e f a b'") {
    new TestTrees {
      val t3 = List('a','b','a','c', 'a', 'a', 'd', 'e', 'f', 'a', 'b')
//      println(times(t3))
      assert(times(t3) === List(('b',2), ('a',5), ('f',1), ('e',1), ('d',1), ('c',1)))
    }
  }

  test("makeOrderedLeafList for some frequency table") {
    assert(makeOrderedLeafList(List(('t', 2), ('e', 1), ('x', 3))) === List(Leaf('e',1), Leaf('t',2), Leaf('x',3)))
  }


  test("combine of some leaf list") {
    new TestTrees {
      assert(combine(leaflist) === List(Leaf('x', 3), Fork(Leaf('e', 2), Leaf('t', 2), List('e', 't'), 4)))
      assert(combine(leaflist2) === List(Leaf('x', 3), Fork(Leaf('e', 2), Leaf('t', 2), List('e', 't'), 4), Leaf('d', 5), Leaf('b', 7)))
    }
  }

  test("until singleton") {
    new TestTrees {
      assert(until(singleton, combine)(leaflist) === List(Fork(Leaf('x',3),Fork(Leaf('e',2),Leaf('t',2),List('e', 't'),4),List('x', 'e', 't'),7)))
    }
  }

  test("create code tree") {
    new TestTrees {
      assert(createCodeTree(List('e','t','t','x','x','x','x')) === Fork(Fork(Leaf('e', 1), Leaf('t', 2), List('e', 't'), 3), Leaf('x', 4), List('e', 't', 'x'), 7))
    }
  }

  test("decode code tree") {
    new TestTrees {
      assert(decode(createCodeTree(List('e','t','t','x','x','x','x')), List(0,0,1)) === List('e','x'))
    }
  }

  println("French secret : " + decodedSecret)

  test("decode and encode a very short text should be identity") {
    new TestTrees {
      assert(decode(t1, encode(t1)("ab".toList)) === "ab".toList)
    }
  }

  test("codeBits test") {
    new TestTrees {
      val table = List(('a',List(1,1,0)), ('b',List(0,1)), ('c',List(1,0)))
      assert(codeBits(table)('a') === List(1,1,0))
      assert(codeBits(table)('b') === List(0,1))
      assert(codeBits(table)('c') === List(1,0))
    }
  }

  test("convert test") {
    new TestTrees {
      val table = convert(createCodeTree(List('e','t','t','x','x','x','x')))
      assert(table === List(('e',List(0, 0)), ('t',List(0, 1)), ('x',List(1))))
    }
  }

  test("quickEncode test") {
    new TestTrees {
      assert(quickEncode(createCodeTree(List('e','t','t','x','x','x','x')))("tex".toList) === List(0, 1, 0, 0, 1))
    }
  }
}
